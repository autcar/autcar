from autcar import Camera, Car, RemoteController, Capture
import time

rc = RemoteController(port=9988)
# car = Car()
# cam = Camera(rotation=-1)
# cam.start() #start camera to see pictures during driving
# cap = Capture(car, cam, capture_interval=1)

rc.listen()
direction = None

while True:
    cmd = rc.get_cmds()
    print(cmd + ", time:" + str(int(time.time())))
    if(cmd == "fast"):
        direction = "forward"
        # car.move("forward", "medium")
    if(cmd == "stop"):
        # car.stop()
        direction = "stop"
    if(cmd == "faster"):
        direction = "forward"
        # car.move("forward", "fast")
    if(cmd == "backwards"):
        direction = "backwards"
        # car.move("backwards")
    if(cmd == "leftlight"):
        pass
        # car.left("light", direction)
    if(cmd == "lefthard"):
        pass
        # car.left("medium", direction)
    if(cmd == "rightlight"):
        pass
        # car.right("light", direction)
    if(cmd == "righthard"):
        pass
        # car.right("medium", direction)
    if(cmd == "startrecording"):
        pass
        # cap.start()
    if(cmd == "stoprecording"):
        pass
        # cap.stop()
        
