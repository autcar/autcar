import ast
import csv
import datetime
import io
import itertools
import os
import numpy as np
import shutil
import random
import onnxruntime as rt
import matplotlib.pyplot as plt
from tensorflow import summary, image as im, expand_dims
from sklearn.metrics import confusion_matrix
from PIL import Image, ImageFile
from typing import List, Union

os.environ['TF_KERAS'] = '1'
ImageFile.LOAD_TRUNCATED_IMAGES = True


class Trainer:

    def __init__(self, deeplearning_framework="keras", image_width=224, image_height=168):
        """
        A trainer object that is used to prepare the dataset, train and test the model
        """
        self.__image_width = image_width
        self.__image_height = image_height
        self.__deeplearning_framework = deeplearning_framework
        self.__commands = ['left_light_backwards', 'left_light_forward', 'left_medium_backwards', 'left_medium_forward',
                           'move_fast_forward', 'move_medium_backwards', 'move_medium_forward', 'right_light_backwards',
                           'right_light_forward', 'right_medium_backwards', 'right_medium_forward', 'stop']

    def __scale_image(self, image, width=None, height=None):

        if width is None:
            width = self.__image_width
        if height is None:
            height = self.__image_height

        try:
            return image.resize((width, height), Image.LINEAR)
        except:
            raise Exception('scale_image error')

    def plot_to_image(self, figure):
        """Converts the matplotlib plot specified by 'figure' to a PNG image and
        returns it. The supplied figure is closed and inaccessible after this call."""
        # Save the plot to a PNG in memory.
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        # Closing the figure prevents it from being displayed directly inside
        # the notebook.
        plt.close(figure)
        buf.seek(0)
        # Convert PNG buffer to TF image
        image = im.decode_png(buf.getvalue(), channels=4)
        # Add the batch dimension
        image = expand_dims(image, 0)
        return image

    def create_balanced_dataset(self, input_folder_path: Union[str, List[str]],
                                output_folder_path: str = 'balanced_dataset', train_test_split: float = 0.8):
        image_counter = 0
        command_counter_start = {}
        command_counter = {}
        ignore_list = ["stop"]

        outputfolder_path = output_folder_path.rstrip('/')
        if os.path.exists(outputfolder_path):
            shutil.rmtree(outputfolder_path)

        os.makedirs(outputfolder_path)

        if type(input_folder_path) == str:
            files = [input_folder_path]
        elif isinstance(input_folder_path, list):
            files = input_folder_path

        for file in files:
            file = file.rstrip('/')
            try:
                with open(file + "/training.csv") as training_file:
                    csv_reader = csv.reader(training_file, delimiter=';')

                    for row in csv_reader:
                        try:
                            command = ast.literal_eval(row[1])
                        except Exception as e:
                            continue
                        cmd_type = command["type"]
                        if cmd_type == "move":
                            cmd_type = command["type"] + "_" + command["speed"] + "_" + command["direction"]
                        if cmd_type == "left" or cmd_type == "right":
                            cmd_type = command["type"] + "_" + command["style"] + "_" + command["direction"]
                        if cmd_type in ignore_list:
                            continue

                        if command_counter_start.get(cmd_type, 0) == 0:
                            command_counter_start[cmd_type] = 1
                        else:
                            command_counter_start[cmd_type] = command_counter_start[cmd_type] + 1

                    training_file.seek(0)

            except Exception as e:
                raise Exception(
                    "No training.csv file found in folder " + file + ". Does the directory path you provided resolve to a training folder created by AutCar?")

        max_class = max(command_counter_start, key=lambda x: command_counter_start.get(x))
        maximum = command_counter_start[max_class]

        train_file = open(outputfolder_path + "/train_map.txt", "w+")
        test_file = open(outputfolder_path + "/test_map.txt", "w+")

        for file in files:
            file = file.rstrip('/')
            with open(file + "/training.csv") as training_file:
                csv_reader = csv.reader(training_file, delimiter=';')
                for row in csv_reader:
                    image = row[0]
                    try:
                        command = ast.literal_eval(row[1])
                    except Exception as e:
                        continue
                    cmd_type = command["type"]
                    if cmd_type == "move":
                        cmd_type = command["type"] + "_" + command["speed"] + "_" + command["direction"]
                    if cmd_type == "left" or cmd_type == "right":
                        cmd_type = command["type"] + "_" + command["style"] + "_" + command["direction"]
                    if cmd_type in ignore_list:
                        continue

                    if command_counter.get(cmd_type, 0) == 0:
                        command_counter[cmd_type] = 1
                    else:
                        command_counter[cmd_type] = command_counter[cmd_type] + 1

                    shutil.copy(file + "/" + image, outputfolder_path + "/image_" + str(image_counter) + ".png")
                    info = outputfolder_path + "/image_" + str(image_counter) + ".png" + "\t" + str(cmd_type) + "\n"
                    if random.uniform(0, 1) < train_test_split:
                        train_file.write(info)
                    else:
                        test_file.write(info)
                    image_counter = image_counter + 1

                training_file.seek(0)

        finished = False
        while not finished:
            for file in files:
                if finished == True:
                    break
                file = file.rstrip('/')
                with open(file + "/training.csv") as training_file:
                    csv_reader = csv.reader(training_file, delimiter=';')
                    for row in csv_reader:
                        image = row[0]
                        try:
                            command = ast.literal_eval(row[1])
                        except Exception as e:
                            continue
                        cmd_type = command["type"]
                        if cmd_type == "move":
                            cmd_type = command["type"] + "_" + command["speed"] + "_" + command["direction"]
                        if cmd_type == "left" or cmd_type == "right":
                            cmd_type = command["type"] + "_" + command["style"] + "_" + command["direction"]
                        if cmd_type in ignore_list:
                            continue

                        if command_counter[cmd_type] < maximum:
                            command_counter[cmd_type] = command_counter[cmd_type] + 1
                            shutil.copy(file + "/" + image, outputfolder_path + "/image_" + str(image_counter) + ".png")
                            info = outputfolder_path + "/image_" + str(image_counter) + ".png" + "\t" + str(
                                cmd_type) + "\n"
                            if random.uniform(0, 1) < train_test_split:
                                train_file.write(info)
                            else:
                                test_file.write(info)
                            image_counter = image_counter + 1

                    finished = True
                    for key, value in command_counter.items():
                        if (value < maximum):
                            finished = False
                            break

        train_file.close()
        test_file.close()

        return True

    def get_classes(self, path_to_folder: str):
        path_to_folder = path_to_folder.rstrip('/')
        classes_dict = dict()
        map_file_train = path_to_folder + "/train_map.txt"

        try:
            with open(map_file_train) as f:
                csv_reader = csv.reader(f, delimiter='\t')
                for row in csv_reader:
                    cmd = row[1]
                    if cmd in classes_dict:
                        classes_dict[cmd] += 1
                    else:
                        classes_dict[cmd] = 1

        except Exception as e:
            raise Exception(
                "No train_map.txt file found in path " + path_to_folder + ". Did you create a dataset using create_balanced_dataset()?")

        return classes_dict

    def train(self, path_to_folder: str, model_definition, epochs: int = 10,
              output_model_path: str = "driver_model.onnx", classes=None, minibatch_size: int = 64):
        if classes is None:
            classes = self.__commands

        self.__train_keras(path_to_folder, model_definition, epochs, output_model_path, classes, minibatch_size)

    def __train_keras(self, path_to_folder: str, model_definition, epochs: int, output_model_path: str, classes,
                      minibatch_size: int):

        def plot_confusion_matrix(cm, class_names):
            """
            Returns a matplotlib figure containing the plotted confusion matrix.

            Args:
              cm (array, shape = [n, n]): a confusion matrix of integer classes
              class_names (array, shape = [n]): String names of the integer classes
            """
            figure = plt.figure(figsize=(8, 8))
            plt.imshow(cm, interpolation='nearest', cmap=plt.cm.get_cmap("Blues"))
            plt.title("Confusion matrix")
            plt.colorbar()
            tick_marks = np.arange(len(class_names))
            plt.xticks(tick_marks, class_names, rotation=45)
            plt.yticks(tick_marks, class_names)

            # Normalize the confusion matrix.
            # cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

            # Use white text if squares are dark; otherwise black.
            threshold = cm.max() / 2.
            for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                color = "white" if cm[i, j] > threshold else "black"
                plt.text(j, i, cm[i, j], horizontalalignment="center", color=color)

            plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label')
            return figure

        from tensorflow.keras.preprocessing.image import ImageDataGenerator
        from tensorflow.keras import Model
        from tensorflow.keras import callbacks
        os.environ['TF_KERAS'] = '1'
        import onnxmltools
        import pandas as pd

        model: Model = model_definition

        path_to_folder = path_to_folder.rstrip('/')
        map_file_train = path_to_folder + "/train_map.txt"
        map_file_test = path_to_folder + "/test_map.txt"
        minibatch_size = 16

        df_train = pd.read_csv(map_file_train, sep='\t', dtype=str, header=None, names=["filename", "class"])
        df_test = pd.read_csv(map_file_test, sep='\t', dtype=str, header=None, names=["filename", "class"])

        df_train_len = len(df_train)
        df_test_len = len(df_test)

        train_datagen = ImageDataGenerator(samplewise_center=True, samplewise_std_normalization=True,
                                           data_format="channels_first")
        test_datagen = ImageDataGenerator(samplewise_center=True, samplewise_std_normalization=True,
                                          data_format="channels_first")

        generator_train = train_datagen.flow_from_dataframe(df_train, classes=classes,
                                                            target_size=(self.__image_height, self.__image_width),
                                                            shuffle=True)
        generator_test = test_datagen.flow_from_dataframe(df_test, classes=classes,
                                                          target_size=(self.__image_height, self.__image_width),
                                                          shuffle=True)

        model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        file_writer_cm = summary.create_file_writer(log_dir + '/cm')

        def log_confusion_matrix(epoch, logs):

            path_to_test_map = path_to_folder + "/test_map.txt"

            images = []
            ground_truth = []
            predictions = []
            try:
                with open(path_to_test_map) as csv_file:
                    csv_reader = csv.reader(csv_file, delimiter='\t')
                    for row in csv_reader:
                        images.append(row[0])
                        ground_truth.append(row[1])
            except:
                raise Exception("Could not parse testmap. Did you provide a path to a valid test_map.txt file?")

            # Use the model to predict the values from the validation dataset.
            # test_pred_raw = model.predict(test_images)
            # test_pred = np.argmax(test_pred_raw, axis=1)

            class_names = ["left_light_forward",
                           "left_medium_forward",
                           "move_fast_forward",
                           "move_medium_backwards",
                           "move_medium_forward",
                           "right_light_forward",
                           "right_medium_forward",
                           "started"]

            class_names = self.get_classes(path_to_folder)

            test_pred_raw = model.predict(generator_test)
            test_pred = np.argmax(test_pred_raw, axis=1)
            test_labels = generator_test.labels
            print(test_pred_raw)
            print(test_pred.flatten().tolist())
            print(test_labels)

            # Calculate the confusion matrix.
            cm = confusion_matrix(test_labels, test_pred.flatten().tolist())
            # Log the confusion matrix as an image summary.
            figure = plot_confusion_matrix(cm, class_names=class_names)
            cm_image = self.plot_to_image(figure)

            # Log the confusion matrix as an image summary.
            with file_writer_cm.as_default():
                summary.image("Confusion Matrix", cm_image, step=epoch)

        # Define the per-epoch callback.
        cm_callback = callbacks.LambdaCallback(on_epoch_end=log_confusion_matrix)

        print("Training started")

        res = model.fit(generator_train, epochs=epochs,
                        validation_data=generator_test,
                        callbacks=[tensorboard_callback, cm_callback])

        print("\nBatch Train %d / %d = %d" % (df_train_len, minibatch_size, round(df_train_len / minibatch_size)))
        print("\nBatch Test %d / %d = %d" % (df_test_len, minibatch_size, round(df_test_len / minibatch_size)))
        print("\nHistory dict:", res.history)

        model.save_weights(filepath="weights")

        validation_loss = res.history["val_loss"]
        validation_accuracy = res.history["val_accuracy"]

        timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")

        plt.plot(res.history["accuracy"])
        plt.plot(res.history["val_accuracy"])
        plt.title("model accuracy" + "\n(%d training samples)" % df_train_len + " | (%d test samples)" % df_test_len)
        plt.xlabel("epoch")
        plt.ylabel("accuracy")
        plt.legend(['train', 'test'], loc="upper left")
        plt.savefig("../plot_acc_%s.png" % timestamp)
        plt.close()
        # plt.show()

        plt.plot(res.history["loss"])
        plt.plot(res.history["val_loss"])
        plt.title("model loss" + "\n(%d training samples)" % df_train_len + " | (%d test samples)" % df_test_len)
        plt.xlabel("epoch")
        plt.ylabel("loss")
        plt.legend(['train', 'test'], loc="upper left")
        plt.savefig("../plot_loss_%s.png" % timestamp)
        plt.close()
        # plt.show()

        print("\nHistory dict:", res.history)

        onnx_model = onnxmltools.convert_keras(model)
        onnxmltools.utils.save_model(onnx_model, output_model_path)

        return validation_loss, validation_accuracy

    def __plot_test(self, confusion_matrix):
        # plt.figure(1)
        # plt.subplot(211)
        # plt.plot(plot_data["batchindex"], plot_data["avg_loss"], 'b--')
        # plt.xlabel('Minibatch number')
        # plt.ylabel('Loss')
        # plt.title('Minibatch run vs. Training loss ')

        timestamp = datetime.datetime.now().strftime("%Y%m%d-%H%M")

        fig, ax = plt.subplots(constrained_layout=True)
        fig.patch.set_visible(False)
        ax.axis('off')
        ax.axis('tight')

        df = confusion_matrix.stats()["class"]

        # 10 = recall, 12 = precision, 17 = accuracy, 18 = F1-score
        ndf = df.ix[[10, 12, 17, 18]]
        ndf.round(3)

        ndf['Scores'] = ["Recall", "Precision", "Accuracy", "F1-Score"]

        table = ax.table(cellText=ndf.values, colLabels=ndf.columns, loc='center')
        table.auto_set_font_size(False)
        table.set_fontsize(6)
        table.auto_set_column_width([0, 1, 2, 3, 4, 5, 6, 7])
        # table.scale(2, 2)  # may help

        # fig.tight_layout()
        plt.savefig("../plot_table_%s.png" % timestamp)
        plt.close()

        confusion_matrix.plot()
        plt.savefig("../plot_matrix_%s.png" % timestamp)
        plt.close()
        # plt.show()

    def test(self, path_to_model: str, path_to_test_map: str):

        from pandas_ml import ConfusionMatrix

        images = []
        ground_truth = []
        predictions = []
        try:
            with open(path_to_test_map) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter='\t')
                for row in csv_reader:
                    images.append(row[0])
                    ground_truth.append(row[1])
        except:
            raise Exception("Could not parse testmap. Did you provide a path to a valid test_map.txt file?")

        sess = rt.InferenceSession(path_to_model)
        input_name = sess.get_inputs()[0].name
        label_name = sess.get_outputs()[0].name

        for i, image in enumerate(images):
            try:
                img = Image.open(image)
            except Exception as e:
                print("Cant read " + image)
            try:
                processed_image = self.__scale_image(img)
            except Exception as e:
                print("Err while reading " + image + ": " + str(e))

            X = np.array([np.moveaxis((np.array(processed_image).astype('float32')), -1, 0)])
            X -= np.mean(X, keepdims=True)
            X /= (np.std(X, keepdims=True) + 1e-6)

            pred = sess.run([label_name], {input_name: X.astype(np.float32)})[0]
            index = int(np.argmax(pred))
            predictions.append(self.__commands[index])

        confusion_matrix = ConfusionMatrix(ground_truth, predictions)
        # print("Confusion matrix:\n%s" % confusion_matrix)

        self.__plot_test(confusion_matrix)

